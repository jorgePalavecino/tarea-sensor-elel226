import processing.sound.*;   //libreria llamada sound.
SoundFile file;
SoundFile file1;

// Sensores de presión y temperatura 
float x=10;
float y=10;
PFont f;
float angleRotate = 0.0;
void setup(){
  size(1920, 1080);//Tamaño pantalla completa.
  file = new SoundFile(this, "008522530_prev.wav");
  file1 = new SoundFile(this, "008522530_prev.wav");
}

//Dibujo de sensores. 
void draw(){
  colorMode(HSB,360,100,100,100);  // Modo de color SHB
  background(255);                // Fondo de pantalla
  // Titulos de los sensores.
    textSize(40); 
    text("Sensor de Presion",30,40);
    text("Sensor de Temperatura",width/2 +30,40);
    
  //Line que divide la pantalla.  
  strokeWeight(10);
  stroke(0);
  line(960,0,960,1080); 
  
  //Marcas, nivel de presion y temperatura (circulos)
  noFill();
  stroke(0);
  strokeWeight(2);
  textSize(20);
   for (int i = 0; i < 1000; i = i+100) { 
      noFill(); 
      ellipse(width/4,width/4, i, i); 
          fill(0);
          text(int(i/100)+"atm",width/4 - i/2,width/4);
          text(int(i/100)+"atm",width/4 + i/2,width/4);
          text(int(i/100)+"atm",width/4,width/4 - i/2);
          text(int(i/100)+"atm",width/4,width/4 + i/2);
      noFill();    
      ellipse(3*width/4,width/4,i/2, i/2);
          fill(0);
          text(int(i/10)+"°C",3*width/4,width/4 - i/4);
          text(int(i/10)+"°C",3*width/4,width/4 + i/4);
      }
  
  //Circulo que marca cual es el nivel de presion o temperatura     
    strokeWeight(0); 
    fill(125+x/3,100,100,75);
    ellipse(width/4,width/4,x, x); //Circulo a la irquiera de la pantalla.
    fill(125+y/3,100,100,75);
    ellipse(3*width/4,width/4,y/2, y/2); //Circulo a la derecha de la pantalla.
  
  //Zona sensible de los sensores demarcacion.
  noFill();
  strokeWeight(10);
  rect(640,910,300,120);
    fill(0);
    stroke(0);
    text("Zona sensible",720,960); 
    text("sensor de presion",690,980);
  noFill();  
  rect(1600,910,300,120);
    fill(0);
    stroke(0);
    text("Zona sensible",1680,960); 
    text("sensor de temperatura",1650,980);
  
 //Momento en el cual empieza a zonal la alarma 
 if(x < 800){
    file.stop();
  }
  if(x >= 800){
    if (!file.isPlaying()){
    file.play();
    }
  }   
  if(y < 800){
    file1.stop();
  }
  if(y >= 800){
    if (!file1.isPlaying()){
    file1.play();
    }
  }
  
//zona de sensibilidad al mouse y limite de aumento.  
if(640 <= mouseX & mouseX <=940 & 910 <= mouseY & mouseY <=1030 ){
   x +=0.5;
      if(x > 900){
      x =900;
      rect(width/4 -150,width/4 -100,300,200);
      fill(#FF0000);
      text("¡PELIGRO!",width/4 -50,width/4);    //Cartel de peligro del sensor de la izquieda. 
      noFill();
      }
  }  else{
      if(x > 10){                               // Este if permite que el circulo no tome valores negativos.
      x -=0.5;                                  // Decreciemiento de la circunferencia izquierda 
      }
    }
  
  if(1600 <= mouseX & mouseX <=1900 & 910 <= mouseY & mouseY <=1030 ){
   y +=0.5;
      if(y > 900){                                     
      y = 900;
      rect(3*width/4 -100,width/4 -50,200,100);
      fill(#FF0000);
      text("¡PELIGRO!",3*width/4 -50,width/4);     //Cartel de peligro del sensor de la derecha.
      noFill();
      }
  } else{
      if(y > 10){                         // Este if permite que el circulo no tome valores negativos.
      y -=0.5;                            // Decreciemiento de la circunferencia derecha.
      }
    }    
}
